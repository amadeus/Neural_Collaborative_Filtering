# Description

This project aims to build a recommender system for movies [Movielens data](https://grouplens.org/datasets/movielens/) based on [Neural Collaborative Filtering Framework](https://www.comp.nus.edu.sg/~xiangnan/papers/ncf.pdf)

The idea here is not to predict the rating that a user give for a given movie, but predict if he gives a rating or not (We considered the implicit feedback recommendation problem)

The data transformation was made using [numpy](http://www.numpy.org/) [pandas](https://pandas.pydata.org/) and can be found [here](https://gitlab.eurecom.fr/amadeus/Neural_Collaborative_Filtering/blob/master/Data_Preprocessing.ipynb).

* The implementation was done in three different steps using [Tensorflow api](https://www.tensorflow.org/):
    * Implementation of [GMF Model](https://gitlab.eurecom.fr/amadeus/Neural_Collaborative_Filtering/blob/master/GMF_Model.ipynb)
    * Implementation of [MLP Model](https://gitlab.eurecom.fr/amadeus/Neural_Collaborative_Filtering/blob/master/MLP_X_Model.ipynb)
    * Implementation of [NeuCF Model](https://gitlab.eurecom.fr/amadeus/Neural_Collaborative_Filtering/blob/master/Neu_MF_Model.ipynb)

As input, we have the one hot encoded vector of the user i and the item j.

As output, we predict if the user i gave a rating to the item j (see the paper)

We evaluate the performance of the algorithm using Hitratio and NDCG metrics as in the paper.